#pragma once

#include <vector>       //used to store multiple quaternions
#include "quaternion.h" //norm defined on vector of quaternion
#include <exception>
#include <stdexcept>


using namespace std;

//EL1NORM (L1) CLASS
//simulates the mathmatical concept of an L1 Norm
//  performed on a set of quaternions
class el1norm
{
private:
    //PRE   mag() is defined for quaternion x
    //PST   returns the value of mag() called on quaternion paramater x
    template<typename T> const T process_element(const quaternion<T>& x) const;

public:
    //PRE   vector contains at least 1 quaternion
    //PST   returns the norm of the quaternion set
    //EXC   throws std::runtime_error() if vector is empty
    template<class T> const T operator()(const vector<quaternion<T>> &vect) const;


};

#include "el1norm.hpp"

