//Geoffrey Cline
//CS5201, HW1, SS17
//Quaternion Class

#pragma once

#include <iostream> //defines stream operators
#include <cmath>    //sqrt() and pow() used in math functions
#include <vector>   //used for reading in from file
#include <sstream>  //used for input processing
#include <fstream>  //used for reading in from file
#include <exception>
#include <stdexcept>


using namespace std;

//QUATERNION CLASS
//simulates the mathmatical concept of a quaternion
//
//templating on a type T requires:
//  Operators Defined:  + += -= * / == =
//                      -, both versions
//                      << >> for streams
//  Compatible with:    std::pow()
//                      std::sqrt()
//  Assumes:            L1 norm of null set is zero
//                      coefficents can be set to zero
//
//  Input validation is ONLY provided when T is
//      either int or float. When using other types,
//      behavior may be undefined. See fileInput()
//      and operator>> for more information.
//
template<class T>
class quaternion
{
private:
    //member variables for coefficents of quaternion
    T m_a, m_b, m_c, m_d;


    //PRE   std::pow() and std::sqrt() are compatible with T
    //PST   returns the norm of the quaternion
    const T nnorm() const;

    //PRE
    //PST   returns a modifiable reference to the member variable
    //      indexed on 0, for a...d
    //EXC   throws std::runtime_error() for invalid access
    T & element(const int i);

public:

    //PRE   type T can be initalized to zero
    //PST   quaternion initalized to zero
    quaternion(): m_a(0), m_b(0), m_c(0), m_d(0) {}

    //PRE   paramaters can be converted to type T
    //PST   paramaters initalized to paramaters
    quaternion(const T& a, const T& b, const T& c, const T& d);

    //PRE
    //PST   quaternion deep copy
    quaternion(const quaternion& tCopy);

    //PRE
    //PST   quaternion destructed
    ~quaternion() {}

    //PRE   operator = defined for type T
    //PST   deep copy of paramater
    const quaternion<T> & operator=(const quaternion<T>& qRHS);

    //PRE   operator += defined for type T
    //PST   += performed on corresponding member variables
    //      calling object updated with result
    quaternion<T> & operator+=(const quaternion<T>& qRHS);

    //PRE   operator -= defined for type T
    //PST   -= performed on corresponding member variables
    //      calling object updated with result
    quaternion<T> & operator-=(const quaternion<T>& qRHS);

    //PRE   operator + defined for T
    //PST   + performed on corresponding member variables
    //      returns object with result
    //      neither calling or paramter changed
    const quaternion<T> operator+(const quaternion<T>& qRHS) const;

    //PRE   operator - defined for T
    //PST   - performed on corresponding member variables
    //      returns object with result
    //      neither calling or paramter changed
    const quaternion<T> operator-(const quaternion<T>& qRHS) const;

    //PRE   operator * defined for T
    //PST   * performed on corresponding member variables
    //      returns object with result
    //      neither calling or paramter changed
    const quaternion<T> operator*(const quaternion<T>& qRHS) const;

    //PRE   operator / defined for T
    //PST   returns calling object multiplied with inverse of qRHS
    //      performed on corresponding member variables
    //      returns object with result
    //      neither calling or paramter changed
    //EXC   throws std::logic_error() if norm is zero.
    const quaternion<T> operator/(const quaternion<T>& qRHS) const;

    //PRE   operator == defined for T
    //PST   == performed on corresponding member variables
    //      returns true if & only if all elements == to true
    const bool operator==(const quaternion<T>& qRHS) const;

    //PRE   operator == defined for T
    //PST   returns negation of operator==()
    const bool operator!=(const quaternion<T> & qRHS) const;

    //PRE
    //PST   returns modifiable reference to coefficent
    //      indexed on 0 for a...d
    //EXC   throws std::runtime_error() for invalid access
    T & operator[](const int i);

    //PRE
    //PST   returns const copy of coefficent
    //      indexed on 0 for a...d
    //EXC   throws std::runtime_error() for invalid access
    const T get(const int i) const;

    //PRE   std::pow() and std::sqrt() are compatible with T
    //PST   returns the norm of the quaternion
    const T operator~() const {return nnorm(); }

    //PRE   operator - defined for T
    //      both subtraction and minus
    //      0 - T is equivalent to -T
    //PST   minus performed on all member variables
    //      returns as seperate object
    const quaternion<T> operator-() const;

    //PRE   operator - defined for T
    //      0 - T is equivalent to -T
    //PST   returns conjugate of calling object as seperate object
    const quaternion<T> operator!() const;

    //PRE   std::pow() and std::sqrt() are compatible with T
    //PST   returns the norm of the quaternion
    const T mag() const { return nnorm(); }

    //PRE   T has * / and == defined
    //PST   returns the inverse of the quaternion as seperate object
    //EXC   throws std::logic_error() if norm is zero
    const quaternion<T> inverse() const;

};

//PRE   T << defined
//PST   outputs to given stream as "A B C D "
template<class T>
ostream& operator<<(ostream& os, const quaternion<T>& qRHS);

//PRE   T >> defined
//      if T is int
//          uses stoi() to convert
//      if T is float
//          uses stof() to convert
//      if not int or float
//          !! no input checking
//          !! behavior may be undefined
//      input format as "A B C D "
//PST   quaternion updated
//EXC   throws std::runtime_error() if int or float can't be converted
//      exceptions may be undefined for other types
template<class T>
istream& operator>>(istream& is, quaternion<T>& qRHS);

//PRE   fname references file
//      first line of input from file must be # of further lines
//      subsequent lines will be processed with quaternion::operator>>
//      input checking provided only for int and float
//PST   updates data vector with quaternions based on input lines
//EXC   std::runtime_error() for unable to open file
//      std::runtime_error() for line unable to be converted
template<typename T>
void fileInput(const string& fname, vector<quaternion<T>> &data);


//PRE   fi references stream
//      first line of input from file must be # of further lines
//      subsequent lines will be processed with quaternion::operator>>
//      input checking provided only for int and float
//PST   updates data vector with quaternions based on input lines
//EXC   std::runtime_error() for line unable to be converted
template<typename T>
void fileInput(istream &fi, vector<quaternion<T>> & data);

//PRE   lhs == 1
//PST   returns inverse of quaternion if defined
//EXC   std::runtime_error() for lhs != 1, for undefined division
//      std::runtime_error() if norm of rhs is zero
template<typename T>
const quaternion<T> operator/(const int &lhs, const quaternion<T> &rhs);


#include "quaternion.hpp"


