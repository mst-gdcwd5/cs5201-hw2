#pragma once

#include "quaternion.h"

using namespace std;

template<typename T>
quaternion<T>::quaternion(const T& a, const T& b, const T& c, const T& d)
{
    m_a = static_cast<T>(a);
    m_b = static_cast<T>(b);
    m_c = static_cast<T>(c);
    m_d = static_cast<T>(d);

}

template<typename T>
quaternion<T>::quaternion(const quaternion &tCopy)
{
    *this = tCopy;
}

template<typename T>
const quaternion<T> & quaternion<T>::operator=(const quaternion<T>& qRHS)
{
    m_a = qRHS.m_a;
    m_b = qRHS.m_b;
    m_c = qRHS.m_c;
    m_d = qRHS.m_d;

    return *this;
}

template<typename T>
quaternion<T> & quaternion<T>::operator+=(const quaternion<T>& qRHS)
{
    m_a += qRHS.m_a;
    m_b += qRHS.m_b;
    m_c += qRHS.m_c;
    m_d += qRHS.m_d;

    return *this;
}

template<typename T>
quaternion<T> & quaternion<T>::operator-=(const quaternion<T>& qRHS)
{
    m_a -= qRHS.m_a;
    m_b -= qRHS.m_b;
    m_c -= qRHS.m_c;
    m_d -= qRHS.m_d;

    return *this;
}

template<typename T>
const quaternion<T> quaternion<T>::operator+(const quaternion<T>& qRHS) const
{
    quaternion<T> result;

    result.m_a = m_a + qRHS.m_a;
    result.m_b = m_b + qRHS.m_b;
    result.m_c = m_c + qRHS.m_c;
    result.m_d = m_d + qRHS.m_d;

    return result;
}

template<typename T>
const quaternion<T> quaternion<T>::operator-(const quaternion<T>& qRHS) const
{
    quaternion<T> result;

    result.m_a = m_a - qRHS.m_a;
    result.m_b = m_b - qRHS.m_b;
    result.m_c = m_c - qRHS.m_c;
    result.m_d = m_d - qRHS.m_d;

    return result;
}

template<typename T>
const quaternion<T> quaternion<T>::operator*(const quaternion<T>& qRHS) const
{
    quaternion<T> result;

    result.m_a  = m_a * qRHS.m_a;
    result.m_a -= m_b * qRHS.m_b;
    result.m_a -= m_c * qRHS.m_c;
    result.m_a -= m_d * qRHS.m_d;

    result.m_b  = m_a * qRHS.m_b;
    result.m_b += m_b * qRHS.m_a;
    result.m_b += m_c * qRHS.m_d;
    result.m_b -= m_d * qRHS.m_c;

    result.m_c  = m_a * qRHS.m_c;
    result.m_c -= m_b * qRHS.m_d;
    result.m_c += m_c * qRHS.m_a;
    result.m_c += m_d * qRHS.m_b;

    result.m_d  = m_a * qRHS.m_d;
    result.m_d += m_b * qRHS.m_c;
    result.m_d -= m_c * qRHS.m_b;
    result.m_d += m_d * qRHS.m_a;

    return result;
}

template<typename T>
const quaternion<T> quaternion<T>::operator/(const quaternion<T>& qRHS) const
{
    quaternion<T> result;

    result = (*this) * qRHS.inverse();

    return result;
}

template<typename T>
const quaternion<T> quaternion<T>::inverse() const
{
    quaternion<T> result = !(*this);
    T rnorm = (*this).nnorm();
    rnorm = rnorm * rnorm;

    if(rnorm == 0)
        throw std::logic_error("inverse undefined, norm is zero");

    result.m_a = result.m_a / rnorm;
    result.m_b = result.m_b / rnorm;
    result.m_c = result.m_c / rnorm;
    result.m_d = result.m_d / rnorm;

    return result;
}

template<typename T>
const bool quaternion<T>::operator==(const quaternion<T>& qRHS) const
{
    bool result = true;

    int i = 0;

    //iterates over 4 member variables
    //breaks on failed comparision
    while(++i < 4 && result)
        result = result && (get(i) == qRHS.get(i));

    return result;
}

template<typename T>
const bool quaternion<T>::operator!=(const quaternion<T>& qRHS) const
{
    return !(*this == qRHS);
}

template<typename T>
T & quaternion<T>::operator[](const int i)
{
    return element(i);

}

template<typename T>
const T quaternion<T>::nnorm() const
{
    //inits sum to square of first element
    T result = static_cast<T>(pow(get(0),2));

    //adds remaining elements
    for(int i = 1; i < 4; i++)
        result += static_cast<T>(pow(get(i),2));

    return static_cast<T>(sqrt(result));
}

template<typename T>
T & quaternion<T>::element(const int i)
{
    if(i > 3)
        throw std::runtime_error("invalid access");

    int ri = i % 4;

    switch(ri)
    {
        case 0:
            return m_a;
            break;
        case 1:
            return m_b;
            break;
        case 2:
            return m_c;
            break;
        case 3:
            return m_d;
            break;
        default:
            return m_a;
            break;
    }

}

template<typename T>
const quaternion<T> quaternion<T>::operator-() const
{
    //constructor defaults to zeros
    quaternion<T> zeroQ;

    return zeroQ - *this;
}

template<typename T>
const quaternion<T> quaternion<T>::operator!() const
{
    quaternion<T> result(-(*this));

    result.m_a = m_a;

    return result;
}

template<typename T>
const T quaternion<T>::get(const int i) const
{
    if(i > 3) //if invalid access
    {
        stringstream ss;
        ss << "invalid access at " << i;
        throw std::runtime_error(ss.str());
    }


    int ri = i % 4;


    switch(ri)
    {
        case 0:
            return m_a;
            break;
        case 1:
            return m_b;
            break;
        case 2:
            return m_c;
            break;
        case 3:
            return m_d;
            break;
        default:
            return m_a;
            break;
    }
}

istream& operator>>(istream& is, quaternion<int>& qRHS)
{
    string input;

    //
    for(int i = 0; i < 4; i++)
    {
        is >> input;
        qRHS[i] = stoi(input);
    }

    return is;
}

istream& operator>>(istream& is, quaternion<float>& qRHS)
{
    string input;

    for(int i = 0; i < 4; i++)
    {
        is >> input;
        qRHS[i] = stof(input);
    }

    return is;
}

template<typename T>
istream& operator>>(istream& is, quaternion<T>& qRHS)
{


    is >> qRHS[0];
    is >> qRHS[1];
    is >> qRHS[2];
    is >> qRHS[3];

    return is;
}

template<typename T>
ostream& operator<<(ostream& os, const quaternion<T>& qRHS)
{
    os << qRHS.get(0);
    os << " " << qRHS.get(1);
    os << " " << qRHS.get(2);
    os << " " << qRHS.get(3);
    os << " ";

    return os;
}

template<typename T>
void fileInput(const string& fname, vector<quaternion<T>> &data)
{
    ifstream fi;
    quaternion<T> build;
    data.clear();

    fi.open(fname);

    //if file inaccessible
    if(!fi.is_open())
        throw std::runtime_error("invalid file access");

    fileInput(fi, data);

    return;
}

template<typename T>
void fileInput(istream &fi, vector<quaternion<T>> & data)
{
    int n;
    quaternion<T> build;
    data.clear();
    stringstream ss;

    fi >> n;

    for(int i = 0; i < n; i++)
    {
        try{
            fi >> build;
            data.push_back(build);
        }
        catch(const std::invalid_argument& e)
        {
            ss << "# invalid quaternion on line: " << (i+2);
            throw std::runtime_error(ss.str());
        }
    }
}

template<typename T>
const quaternion<T> operator/(const int &lhs, const quaternion<T> &rhs)
{
    if(lhs == 1) //if specifies inverse
        return rhs.inverse();
    else
        throw std::runtime_error("undefined division of integer by quaternion");
}












