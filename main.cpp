#include <iostream>
#include "el1norm.hpp"
#include "quaternion.h"
#include <vector>
#include <stdexcept>
#include <exception>


//TODO: UML diagram

int main(int argc, char *argv[]) {
    string ifname;
    vector<quaternion<float>> idata;
    el1norm fn;

    std::cout << "# Hello, World Y'All" << std::endl;

    if( argc < 2)
    {
        cout << "# enter file name: " << endl;
        cin >> ifname;
    }
    else
        ifname = argv[1];

    cout << "# Opening File: " << ifname << endl;

    try
    {
        fileInput(ifname, idata);


        if(idata.size() < 6)
            throw std::runtime_error("insufficient input size for spec tests");


        cout << "# Input Size: " << idata.size() << endl;

        cout << "# L1 norm: " << endl;
        cout << fn(idata) << endl;

        cout << "# h[1] += h[2] " << endl;
        idata[0] += idata[1];
        cout << idata[0] << endl;


        cout << "# -h[1] " << endl;
        cout << -idata[0] << endl;

        cout << "# h[1] * h[3] " << endl;
        cout << idata[0] * idata[2] << endl;

        cout << "# !h[4] " << endl;
        cout << !idata[3] << endl;

        //TODO check syntax
        cout << "# h[5] inverse " << endl;
        cout << idata[4].inverse() << endl;
    }
    catch(const std::runtime_error& e)
    {
        cout << "# runtime error: " << e.what() << endl;
    }
    catch(const std::logic_error& e)
    {
        cout << "# logic error: "  << endl;
    }

    return 0;
}
