#define BOOST_TEST_MODULE quaternion_math_test
#include <boost/test/included/unit_test.hpp>
#include "quaternion.h"
#include "el1norm.h"
#include <sstream>

using namespace std;

const int ERROR_ACCEPT = 1;

BOOST_AUTO_TEST_SUITE( qn_asign )

BOOST_AUTO_TEST_CASE( qn_construct )
{
    quaternion<int> ta(1,2,3,4);
    quaternion<float> tb(1.1,2.2,3.3,4.4);
    quaternion<int> tc;
    quaternion<float> td(tb);

    for( int i = 0; i < 4; i++)
        BOOST_CHECK_EQUAL(i+1, ta[i]);

    for( int i = 0; i < 4; i++)
        BOOST_CHECK_CLOSE(((i+1)*11/10.0), tb[i], ERROR_ACCEPT);

    for( int i = 0; i < 4; i++)
        BOOST_CHECK_EQUAL(0, tc[i]);

    for( int i = 0; i < 4; i++)
        BOOST_CHECK_EQUAL(td[i], tb[i]);

}

BOOST_AUTO_TEST_CASE( qn_assign )
{
    quaternion<int> ta(1,2,3,4);
    quaternion<float> tb(1.1,2.2,3.3,4.4);
    quaternion<int> tc;
    quaternion<float> td;

    tc = ta;
    td = tb;


    for( int i = 0; i < 4; i++)
        BOOST_CHECK_CLOSE(td[i], tb[i], ERROR_ACCEPT);


    for( int i = 0; i < 4; i++)
        BOOST_CHECK_EQUAL(ta[i], tc[i]);

}

BOOST_AUTO_TEST_CASE( qn_fast_add )
{
    quaternion<int> ta(1,2,3,4);
    quaternion<float> tb(1.1,2.2,3.3,4.4);
    quaternion<int> tc(1,1,1,1);
    quaternion<float> td(1.1,1.1,1.1,1.1);

    tc += ta;
    td += tb;


    for( int i = 0; i < 4; i++)
        BOOST_CHECK_CLOSE(td[i], tb[i] + 1.1, ERROR_ACCEPT);


    for( int i = 0; i < 4; i++)
        BOOST_CHECK_EQUAL(ta[i] + 1, tc[i]);

}

BOOST_AUTO_TEST_CASE( qn_fast_subtract )
{
    quaternion<int> ta(3,3,3,3);
    quaternion<float> tb(2.2,2.2,2.2,2.2);
    quaternion<int> tc(1,1,1,1);
    quaternion<float> td(1.1,1.1,1.1,1.1);

    ta -= tc;
    tb -= td;


    for( int i = 0; i < 4; i++)
        BOOST_CHECK_CLOSE(tb[i], 1.1, ERROR_ACCEPT);


    for( int i = 0; i < 4; i++)
        BOOST_CHECK_EQUAL(ta[i], 2);

}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE( qn_math )

BOOST_AUTO_TEST_CASE( qn_math_multiply )
{
    quaternion<int> ta(2,3,4,5);
    quaternion<int> tb(6,7,8,9);
    quaternion<int> tc;
    const quaternion<int> tr(-86,28,48,44);

    tc = ta * tb;

    BOOST_CHECK_EQUAL(tc, tr);

}

BOOST_AUTO_TEST_CASE( qn_math_norm )
{
    quaternion<float> ta(2,3,4,5);

    BOOST_CHECK_CLOSE(ta.mag(), 7.348, ERROR_ACCEPT);

}


BOOST_AUTO_TEST_CASE( qn_math_neg )
{
    quaternion<int> ta(2,3,4,5);
    const quaternion<int> tr(-2,-3,-4,-5);
    quaternion<int> tv;

    tv = -ta;

    BOOST_CHECK_EQUAL(tv, tr);

}

BOOST_AUTO_TEST_CASE( qn_math_conj )
{
    quaternion<int> ta(2,3,4,5);
    const quaternion<int> tr(2,-3,-4,-5);
    quaternion<int> tv;

    tv = !ta;

    BOOST_CHECK_EQUAL(tv, tr);

}

BOOST_AUTO_TEST_CASE( qn_math_inverse )
{
    quaternion<float> ta(2,3,4,5);
    const quaternion<float> tr(0.0370,-0.055555556,-0.0741,-0.0926);
    quaternion<float> tv;

    tv = ta.inverse();

    for(int i = 0; i < 4; i++)
        BOOST_CHECK_CLOSE(tv[i], tr.get(i), ERROR_ACCEPT);



}


BOOST_AUTO_TEST_CASE( qn_math_divide )
{
    quaternion<float> ta(5,6,7,8);
    quaternion<float> tb(2,3,6,5);
    const quaternion<float> tr(1.48649,0.135135,-0.135135,-0.324325);

    quaternion<float> tv;

    tv = ta / tb;

    for(int i = 0; i < 4; i++)
        BOOST_CHECK_CLOSE(tv[i], tr.get(i), ERROR_ACCEPT);

}


BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE( qn_compare )

BOOST_AUTO_TEST_CASE( qn_compare_equal )
{
    quaternion<int> ta(3,3,3,3);
    quaternion<int> tb(3,3,3,3);

    for( int i = 0; i < 4; i++)
        BOOST_CHECK_EQUAL(tb[i] == ta[i], tb == ta);

}

BOOST_AUTO_TEST_CASE( qn_compare_not )
{
    quaternion<int> ta(3,3,3,3);
    quaternion<int> tb(3,4,3,3);

    bool result = true;

    for( int i = 0; i < 4; i++)
        result = result && (ta[i] == tb[i]);

    BOOST_CHECK_EQUAL(!result, ta != tb);

}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE( qn_stream )

BOOST_AUTO_TEST_CASE( qn_cout )
{
    stringstream ss;

    quaternion<int> tb(3,4,5,6);

    ss << tb;

    BOOST_CHECK_EQUAL(ss.str(), "3 4 5 6 ");

}

BOOST_AUTO_TEST_CASE( qn_cin )
{
    stringstream ss("5 6 7 8");

    quaternion<int> tb(3,4,5,6);
    quaternion<int> tc(5,6,7,8);

    ss >> tb;

    BOOST_CHECK_EQUAL(tb, tc);


}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE( qn_file )

BOOST_AUTO_TEST_CASE( qn_fin1 )
{
    vector<quaternion<float>> t_data;
    quaternion<float> t_v0(1,2,3,4);
    quaternion<float> t_v1(0,-1,-2,-3);
    quaternion<float> t_v2(4,5,6,7);
    quaternion<float> t_v3(-2,0,0,3);
    quaternion<float> t_v4(1,-1,-1,1);
    quaternion<float> t_v5(8,8,8,8);
    quaternion<float> t_v6(0,0,0,0);
    quaternion<float> t_v7(0.1,0.2,0.1,0.1);
    quaternion<float> t_v8(2,0,0,0);
    quaternion<float> t_v9(0,1,0,1);

    fileInput("data/t1.txt", t_data);

    BOOST_REQUIRE_EQUAL(t_data.size(), 10);

    BOOST_CHECK_EQUAL(t_data[0], t_v0);
    BOOST_CHECK_EQUAL(t_data[1], t_v1);
    BOOST_CHECK_EQUAL(t_data[2], t_v2);
    BOOST_CHECK_EQUAL(t_data[3], t_v3);
    BOOST_CHECK_EQUAL(t_data[4], t_v4);
    BOOST_CHECK_EQUAL(t_data[5], t_v5);
    BOOST_CHECK_EQUAL(t_data[6], t_v6);
    BOOST_CHECK_EQUAL(t_data[7], t_v7);
    BOOST_CHECK_EQUAL(t_data[8], t_v8);
    BOOST_CHECK_EQUAL(t_data[9], t_v9);


}

BOOST_AUTO_TEST_CASE( qn_fin2 )
{
    stringstream ss;
    vector<quaternion<int>> t_data;
    quaternion<int> te(1,2,3,4);

    ss << "1" << endl;
    ss << "a b 1 2 " << endl;

    try {
        fileInput(ss, t_data);
        //this should be unreachable
        BOOST_CHECK(false);
    }
    catch(const std::runtime_error &e)
    {
        BOOST_CHECK_EQUAL(e.what(),"# invalid quaternion on line: 2");
    }

}

    //TODO: this test is bad.
BOOST_AUTO_TEST_CASE( qn_fin3 )
{
    stringstream ss;
    vector<quaternion<float>> t_data;
    quaternion<float> te(1,2,3,4);

    ss << "1" << endl;
    ss << "2 5 u 2 " << endl;

    try {
        fileInput(ss, t_data);
        //this should be unreachable
        BOOST_CHECK(false);
    }
    catch(const std::runtime_error &e)
    {
        BOOST_CHECK_EQUAL(e.what(),"# invalid quaternion on line: 2");
    }

}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE( norm )

BOOST_AUTO_TEST_CASE( norm_t1 )
{
    vector<quaternion<float>> t_data;
    el1norm fn;
    float result;

    fileInput("data/t2.txt", t_data);

    BOOST_REQUIRE_EQUAL(t_data.size(), 2);

    result = fn(t_data);


    BOOST_CHECK_CLOSE(13.1909+5.47723, result, ERROR_ACCEPT );

}


BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE( sdata )

BOOST_AUTO_TEST_CASE( sdata_t1 )
{
    vector<quaternion<float>> idata;
    quaternion<float> tr1(1,1,1,1);
    quaternion<float> tr2(-1,-1,-1,-1);
    quaternion<float> tr3(-14,10,8,12);
    quaternion<float> tr4(2,0,0,-3);
    quaternion<float> tr5(0.25,0.25,0.25,-0.25);
    fileInput("data/t1.txt", idata);


    idata[0] += idata[1];
    BOOST_CHECK_EQUAL(idata[0],tr1);

    BOOST_CHECK_EQUAL(-idata[0],tr2);

    BOOST_CHECK_EQUAL(idata[0] * idata[2],tr3);

    BOOST_CHECK_EQUAL(!idata[3],tr4);


    BOOST_CHECK_EQUAL(idata[4].inverse(),tr5);

}


BOOST_AUTO_TEST_SUITE_END()












