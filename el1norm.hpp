#pragma once

#include "el1norm.h"

using namespace std;

template<typename T>
const T el1norm::process_element(const quaternion<T>& x) const
{
    return x.mag();
}

template<typename T>
const T el1norm::operator()(const vector<quaternion<T>> &vect) const
{
    if(vect.size() < 1)
        throw std::runtime_error("need at least 1 quaternion for L1 norm");

    auto it = vect.begin();

    // init result with result of first quaternion magnitude
    T result = process_element(*it);

    ++it;

    // add in remaining quaternion magnitudes
    for(; it != vect.end(); ++it)
        result += process_element(*it);

    return result;
}

